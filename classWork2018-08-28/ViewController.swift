//
//  ViewController.swift
//  classWork2018-08-28
//
//  Created by student on 28.08.18.
//  Copyright © 2018 lapin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        let a = arithmetic(5)
        print ("result is \(a)")

    }

    func sayHello() {
        print ("Hello!")
    }

    func dayOfTheWeek(_ number: Int) {
        var day = ""
        if number == 1 || number % 7 == 1{
            day = "Monday"
        }
        if number == 2 || number % 7 == 2{
            day = "Tuesday"
        }
        if number == 3 || number % 7 == 3{
            day = "Wednesday"
        }
        if number == 4 || number % 7 == 4{
            day = "Thursday"
        }
        if number == 5 || number % 7 == 5{
            day = "Friday"
        }
        if number == 6 || number % 7 == 6{
            day = "Saturday"
        }
        if number == 7 || number % 7 == 0{
            day = "Sunday"
        }
        print (day)
    }

    func sum(firstNumber: Int, secondNumber: Int) {
        let result = firstNumber + secondNumber
        print ("sum of \(firstNumber) and \(secondNumber) = \(result)")
    }

    func arithmetic(_ number: Int) -> Int {
        let result = number * 30 + 5
        return result
    }

    func sayHelloCount(_ number: Int) {
        for _ in 0..<number {
            sayHello()
        }
    }

    func fib() {

    }

}

